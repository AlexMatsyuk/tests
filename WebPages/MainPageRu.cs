﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;

namespace Tests.WebPages
{
    class MainPageRu : BaseTestClass
    {
        public static IWebElement Language()
        {
            IWebElement element = driver.FindElement(By.ClassName("langText"));
            return element;
        }
        public static IWebElement English()
        {
            IWebElement element = driver.FindElement(By.LinkText("English"));
            return element;
        }
        public static IWebElement SearchField()
        {
            IWebElement element = driver.FindElement(By.Id("searchinp"));
            return element;
        }
        public static IWebElement FromWhereField()
        {
            IWebElement element = driver.FindElement(By.Id("acFrom"));
            return element;
        }
        public static IWebElement ToWhereField()
        {
            IWebElement element = driver.FindElement(By.Id("acTo"));
            return element;
        }
        public static IWebElement WhenField()
        {
            IWebElement element = driver.FindElement(By.Id("yDate"));
            return element;
        }
        public static IList<IWebElement> Dates()
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.TagName("a.ui-state-default")));
            IList<IWebElement> elements = driver.FindElements(By.TagName("a.ui-state-default"));
            return elements;

        }
        public static IWebElement SearchButton()
        {
            IWebElement element = driver.FindElement(By.CssSelector("#fTickets > div.right-col > div.button-wrapper > span > input[type=submit]"));
            return element;
        }
    }
}
