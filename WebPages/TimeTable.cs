﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Tests.WebPages
{
    class TimeTable : BaseTestClass
    {
        public static IList<IWebElement> Trains()
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.TagName("a.train_text")));
            IList<IWebElement> elements = driver.FindElements(By.TagName("a.train_text"));
            return elements;
        }
        public static IList<IWebElement> StartTime()
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.TagName("b.train_start-time")));
            IList<IWebElement> elements = driver.FindElements(By.TagName("b.train_start-time"));
            return elements;
        }
        public static IWebElement TrainTitle()
        {
            IWebElement element = driver.FindElement(By.CssSelector("body > div.g-wrapper > div.g-wrapper_inner > div.g-main > div.g-content > h2"));
            return element;
        }
        public static IWebElement CalendarDescription()
        {
            IWebElement element = driver.FindElement(By.ClassName("calendar_description"));
            return element;
        }
        public static IWebElement Logo()
        {
            IWebElement element = driver.FindElement(By.ClassName("logo_img"));
            return element;
        }
    }    
}
