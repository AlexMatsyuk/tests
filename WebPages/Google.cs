﻿using OpenQA.Selenium;

namespace Tests.WebPages
{
    class Google : BaseTestClass
    {

        public static IWebElement SearchField()
        {
            IWebElement element = driver.FindElement(By.Name("q"));
            return element;
        }
        public static IWebElement RailwayLink()
        {
            IWebElement element = driver.FindElement(By.PartialLinkText("https://www.rw.by"));
            return element;
        }

    }
}