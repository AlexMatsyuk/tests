﻿using NUnit.Framework;
using OpenQA.Selenium.Firefox;

namespace Tests
{
    [TestFixture]
    [Parallelizable]
    public class Firefox : BaseTestClass
    {
        [OneTimeSetUp]
        public void FirefoxSetUp()
        {
            driver = DriverSetUp(new FirefoxDriver());
        }

        [Test]
        public void Test1()
        {
            Chrome.GoogleVisit();
        }

        [Test]
        public void Test2()
        {
            Chrome.MenuButtonsCheck();
        }

        [Test]
        public void Test3()
        {
            Chrome.SearchCheck();
        }

        [Test]
        public void Test4()
        {
            Chrome.TrainsSearch();
        }
    }
}